function saveOptions(e) {
  e.preventDefault();
  browser.storage.sync.set({
    baseUrl: document.querySelector("#base-url-link").value
  });
}

function restoreOptions() {

  function setCurrentChoice(result) {
    document.querySelector("#base-url-link").value = result.baseUrl || "add Link here";
  }

  function onError(error) {
    console.log(`Error: ${error}`);
  }

  let getting = browser.storage.sync.get("baseUrl");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);