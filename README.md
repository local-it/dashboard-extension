# Dashboard-Extension

As a user I want to easily switch between different applications.


## install plugin 

### xpi

[Click here to add To Firefox](https://git.local-it.org/LIT/dashboard-extension/raw/branch/master/dashboard-1.1-fx.xpi)

### manually

Download zip file from git repository or run `zip -r -FS ../my-extension.zip * --exclude '*.git*'`
in firefox got to `about:addons` click `debug addon`, `load temporary addon` 

USAGE:
Got to Addon Preferences and adjust baseUrl
Press ctrl + Enter on a webpage belonging to your domain to display the application Buttons for Wekan/Nextcloud/authentik
Press the icon on the top right extension bar to open dashboard on any webpage

