
var buttonOffset = 250; //determines where the first of three buttons is rendered 
var baseUrl="default"; //shortcuts should only work when baseUrl is included in current url

document.addEventListener("keyup", (event)  => {
  if (event.ctrlKey && event.code == "Enter") {
    if(document.location.host.includes(baseUrl))
    {
      toggleDisplayBoard();    
    }
  }
});

// displays the board as overlay
function toggleDisplayBoard()
{
  var dashboard = document.querySelector('#lit-dashboard');
    isFront = dashboard.style.display === 'block';
    viewportWidth = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    viewportHeight = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

    dashboard.style.left = (document.documentElement.clientWidth/2) - 1.5*buttonOffset + 'px' ;
    dashboard.style.top = (viewportHeight/2) -  buttonOffset + 'px';
    dashboard.style.position = "fixed";
    dashboard.style.zIndex = "3000";

    dashboard.style.display = isFront ? 'none' : 'block';
}

function onError(error) {
  console.log(`Error: ${error}`);
}

// gets rid of all the Buttons
function deleteButtons(dashboard)
{
  document.getElementById("dashboard-nextcloud-button").remove();
  document.getElementById("dashboard-wekan-button").remove();
  document.getElementById("dashboard-authentik-button").remove();
}
// adds the Buttons and the link prefixes
// button.click() leads to prefix + baseUrl
function createButtons(baseUrl, dashboard)
{

  links= {dashboard: "https://", wekan: "https://board.", nextcloud: "https://cloud." }
  
  // Button to Wekan at links["wekan"]+baseUrl
  var wekanButton = document.createElement("button");
  wekanButton.style.position= "relative";
  wekanButton.style.top= buttonOffset + 'px';
  wekanButton.setAttribute("class", "lit-button wekan-button");
  wekanButton.setAttribute("id", "dashboard-wekan-button");
  wekanButton.addEventListener ("click", function() {
    window.open(links["wekan"] + baseUrl, "_self");
  });

  // Button to authentik at links["dashboard"]+baseUrl
  var authentikButton = document.createElement("button");
  authentikButton.setAttribute("class", "lit-button authentik-button");
  authentikButton.setAttribute("id", "dashboard-authentik-button");
  authentikButton.addEventListener ("click", function() {
    window.open(links["dashboard"] + baseUrl, "_self");
  });
  
  // Button to nextcloud at links["nextcloud"]+baseUrl
  var nextcloudButton = document.createElement("button");
  nextcloudButton.setAttribute("class", "lit-button nextcloud-button");
  nextcloudButton.setAttribute("id", "dashboard-nextcloud-button");
  nextcloudButton.addEventListener ("click", function() {
    window.open(links["nextcloud"] + baseUrl, "_self");
  });

  // Append buttons to "lit-dashboard"
  dashboard.appendChild(nextcloudButton);
  dashboard.appendChild(wekanButton);
  dashboard.appendChild(authentikButton);
}

// creates or finds Dashboard
// basic functionality:
// initializes or finds dashboard div
// prepends new div before document.body.firstChild
function createOrFindDashboard(baseUrl)
{
  if (document.getElementById('lit-dashboard'))
  {
    dashboard = document.getElementById('lit-dashboard');
    dashboard.style.position = "fixed";
    dashboard.style.zIndex='3000';
    dashboard.style.height = '60px';
    dashboard.style.display = 'none';
    deleteButtons(dashboard);
    createButtons(baseUrl, dashboard);
  }
  
  else
  {
    var dashboard = document.createElement("div");
    dashboard.setAttribute("id", "lit-dashboard");
    dashboard.style.position = "fixed";
    dashboard.style.zIndex='3000';
    dashboard.style.height = '60px';
    dashboard.style.display = 'none';
    createButtons(baseUrl, dashboard);
    var currentDiv = document.body.firstChild;
    document.body.insertBefore(dashboard, currentDiv);
  }
}
function onGot(item) {
  if (item.baseUrl) {
    baseUrl = item.baseUrl;
  }
  createOrFindDashboard(baseUrl);
}

let getting = browser.storage.sync.get("baseUrl");
getting.then(onGot, onError);

// listen to message from background.js
browser.runtime.onMessage.addListener(request => {
  if (request.trigger === "extension click")
  {
    toggleDisplayBoard();
    return Promise.resolve({response: ""});
  }
});